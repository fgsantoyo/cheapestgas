package com.fgsantoyo.cheapestgas;

import android.app.Application;
import android.test.ApplicationTestCase;
import android.util.Log;

/**
 * <a href="http://d.android.com/tools/testing/testing_android.html">Testing Fundamentals</a>
 */
public class ApplicationTest extends ApplicationTestCase<Application> {

    private static final String LOG_TAG = ApplicationTest.class.getName();

    public ApplicationTest() {
        super(Application.class);
    }

    public void testSayHello() {

        Log.v(LOG_TAG, "Hello World");
    }

}