package com.fgsantoyo.cheapestgas.service;

import com.fgsantoyo.cheapestgas.domain.GasStationsResponse;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Path;

public interface GasStationService {

    @GET("/stations/radius/{latitude}/{longitude}/{distance}/{fuelType}/{sortBy}/rfej9napna.json")
    void displayGasStations(@Path("latitude") String latitude,
                            @Path("longitude") String longitude,
                            @Path("distance") String distance,
                            @Path("fuelType") String fuelType,
                            @Path("sortBy") String sortBy,
                            Callback<GasStationsResponse> callback);

}
