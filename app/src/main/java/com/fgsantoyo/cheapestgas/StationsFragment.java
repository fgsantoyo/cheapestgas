package com.fgsantoyo.cheapestgas;

import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.fgsantoyo.cheapestgas.domain.GasStationsResponse;
import com.fgsantoyo.cheapestgas.domain.Station;
import com.fgsantoyo.cheapestgas.service.GasStationService;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * A placeholder fragment containing a simple view.
 */
public class StationsFragment extends Fragment {

    private static final String LOG_TAG = StationsFragment.class.getName();

    private ArrayAdapter<String> gasStationsAdapter;

    public StationsFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Add this line in order for this fragment to handle menu events.
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.fragmentmain, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_refresh) {
            //updateList();
            final Location location = ((MainActivity) getActivity()).getLocation();
            Log.d("Fragment", "Lat:" + location.getLatitude() + " Long:" + location.getLongitude());
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        gasStationsAdapter =
                new ArrayAdapter<String>(
                        getActivity(), // The current context (this activity)
                        R.layout.list_item_stations, // The name of the layout ID.
                        R.id.list_item_stations_textview, // The ID of the textview to populate.
                        new ArrayList<String>());

        View rootView = inflater.inflate(R.layout.stations_fragment, container, false);

        ListView stationsListview = (ListView) rootView.findViewById(R.id.listview_stations);
        stationsListview.setAdapter(gasStationsAdapter);
        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        // final Location location = ((MainActivity) getActivity()).getLocation();

        String endPoint = "http://devapi.mygasfeed.com/";

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(endPoint)
                .build();
        GasStationService service = restAdapter.create(GasStationService.class);
        String latitude = "28.75227";
        String longitude = "-81.35699";
        String distance = "25";
        String fuelType = "reg";
        String sortBy = "Price";
        service.displayGasStations(latitude, longitude, distance, fuelType, sortBy,
                new Callback<GasStationsResponse>() {

            @Override
            public void success(GasStationsResponse gasStationResponse, Response response) {
                Log.d(LOG_TAG, "Success");
                if (null != gasStationResponse
                        && null != gasStationResponse.getStatus()
                        && gasStationResponse.getStatus().getCode() == 200
                        && ListUtils.isNotEmpty(gasStationResponse.getStations())) {

                    gasStationsAdapter.clear();
                    for (Station station : gasStationResponse.getStations()) {
                        gasStationsAdapter.add(station.getId() + "-" + station.getStation());
                    }
                } else {
                    Toast.makeText(getActivity(), "Sorry, we could not retrieve gas prices at this moment.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Log.e(LOG_TAG, "Error");
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
    }
}