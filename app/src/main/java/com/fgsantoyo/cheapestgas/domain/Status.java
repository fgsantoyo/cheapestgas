
package com.fgsantoyo.cheapestgas.domain;

import com.google.gson.annotations.Expose;

public class Status {

    @Expose
    private String error;
    @Expose
    private Integer code;
    @Expose
    private String description;
    @Expose
    private String message;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
