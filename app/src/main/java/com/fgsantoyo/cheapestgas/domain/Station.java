
package com.fgsantoyo.cheapestgas.domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Station {

    @Expose
    private String country;
    @SerializedName("reg_price")
    @Expose
    private String regPrice;
    @SerializedName("mid_price")
    @Expose
    private String midPrice;
    @SerializedName("pre_price")
    @Expose
    private String prePrice;
    @SerializedName("diesel_price")
    @Expose
    private String dieselPrice;
    @Expose
    private String address;
    @Expose
    private String diesel;
    @Expose
    private String id;
    @Expose
    private String lat;
    @Expose
    private String lng;
    @Expose
    private String station;
    @Expose
    private String region;
    @Expose
    private String city;
    @SerializedName("reg_date")
    @Expose
    private String regDate;
    @SerializedName("mid_date")
    @Expose
    private String midDate;
    @SerializedName("pre_date")
    @Expose
    private String preDate;
    @SerializedName("diesel_date")
    @Expose
    private String dieselDate;
    @Expose
    private String distance;

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getRegPrice() {
        return regPrice;
    }

    public void setRegPrice(String regPrice) {
        this.regPrice = regPrice;
    }

    public String getMidPrice() {
        return midPrice;
    }

    public void setMidPrice(String midPrice) {
        this.midPrice = midPrice;
    }

    public String getPrePrice() {
        return prePrice;
    }

    public void setPrePrice(String prePrice) {
        this.prePrice = prePrice;
    }

    public String getDieselPrice() {
        return dieselPrice;
    }

    public void setDieselPrice(String dieselPrice) {
        this.dieselPrice = dieselPrice;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDiesel() {
        return diesel;
    }

    public void setDiesel(String diesel) {
        this.diesel = diesel;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getStation() {
        return station;
    }

    public void setStation(String station) {
        this.station = station;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getRegDate() {
        return regDate;
    }

    public void setRegDate(String regDate) {
        this.regDate = regDate;
    }

    public String getMidDate() {
        return midDate;
    }

    public void setMidDate(String midDate) {
        this.midDate = midDate;
    }

    public String getPreDate() {
        return preDate;
    }

    public void setPreDate(String preDate) {
        this.preDate = preDate;
    }

    public String getDieselDate() {
        return dieselDate;
    }

    public void setDieselDate(String dieselDate) {
        this.dieselDate = dieselDate;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

}
