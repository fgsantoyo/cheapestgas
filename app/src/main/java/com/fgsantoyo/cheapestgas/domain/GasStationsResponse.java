
package com.fgsantoyo.cheapestgas.domain;

import com.google.gson.annotations.Expose;

import java.util.ArrayList;
import java.util.List;

public class GasStationsResponse {

    @Expose
    private Status status;
    @Expose
    private GeoLocation geoLocation;
    @Expose
    private List<Station> stations = new ArrayList<Station>();

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public GeoLocation getGeoLocation() {
        return geoLocation;
    }

    public void setGeoLocation(GeoLocation geoLocation) {
        this.geoLocation = geoLocation;
    }

    public List<Station> getStations() {
        return stations;
    }

    public void setStations(List<Station> stations) {
        this.stations = stations;
    }

}
