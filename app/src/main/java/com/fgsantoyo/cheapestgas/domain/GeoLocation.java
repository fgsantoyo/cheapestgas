
package com.fgsantoyo.cheapestgas.domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GeoLocation {

    @SerializedName("city_id")
    @Expose
    private String cityId;
    @SerializedName("city_long")
    @Expose
    private String cityLong;
    @SerializedName("region_short")
    @Expose
    private String regionShort;
    @SerializedName("region_long")
    @Expose
    private String regionLong;
    @SerializedName("country_long")
    @Expose
    private String countryLong;
    @SerializedName("country_id")
    @Expose
    private String countryId;
    @SerializedName("region_id")
    @Expose
    private String regionId;

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getCityLong() {
        return cityLong;
    }

    public void setCityLong(String cityLong) {
        this.cityLong = cityLong;
    }

    public String getRegionShort() {
        return regionShort;
    }

    public void setRegionShort(String regionShort) {
        this.regionShort = regionShort;
    }

    public String getRegionLong() {
        return regionLong;
    }

    public void setRegionLong(String regionLong) {
        this.regionLong = regionLong;
    }

    public String getCountryLong() {
        return countryLong;
    }

    public void setCountryLong(String countryLong) {
        this.countryLong = countryLong;
    }

    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    public String getRegionId() {
        return regionId;
    }

    public void setRegionId(String regionId) {
        this.regionId = regionId;
    }

}
